package com.cognizant.account.dao;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.cognizant.account.model.Account;

@Component
public class AccountDao {

	ApplicationContext context;
	Account account;
	public AccountDao() {
		super();
		// TODO Auto-generated constructor stub
		context = new ClassPathXmlApplicationContext("account.xml");
		account = context.getBean("account",Account.class);
	}
	
	public Account getAccount(String number) {
		Account dummyAccount = new Account();
		dummyAccount.setNumber(number);
		
		if(account.equals(dummyAccount))
			return account;
		
		return null;
	}

	
}
