package com.cognizant.loan.dao;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.cognizant.loan.model.Loan;

@Component
public class LoanDao {

	ApplicationContext context;
	private Loan loan;
	public LoanDao() {
		super();
		// TODO Auto-generated constructor stub
		context = new ClassPathXmlApplicationContext("loan.xml");
		loan = context.getBean("loan",Loan.class);
	}
	
	public Loan getLoan(String number) {
		Loan dummyLoan = new Loan();
		dummyLoan.setNumber(number);
		
		if(loan.equals(dummyLoan))
			return loan;
		
		return null;
	}
	
	
}
