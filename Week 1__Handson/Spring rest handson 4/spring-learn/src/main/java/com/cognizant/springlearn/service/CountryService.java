package com.cognizant.springlearn.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.cognizant.springlearn.Country;
import com.cognizant.springlearn.exception.CountryNotFoundException;

@Service
public class CountryService {

	public Country getCountry(String code) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("country.xml");
		List<Country> countries = context.getBean("countryList", ArrayList.class);
		Country countryCode = new Country();
		countryCode.setCode(code);
		
		if(!countries.contains(countryCode)) {
			System.out.println("inside exception");
			throw new CountryNotFoundException();
		}else {
			Iterator iterator = countries.iterator();
			while(iterator.hasNext()) {
				Country country = (Country)iterator.next();
				if(country.getCode().equals(code))
					return country;
			}
		}
		
		
		return null;
		
	}
}
