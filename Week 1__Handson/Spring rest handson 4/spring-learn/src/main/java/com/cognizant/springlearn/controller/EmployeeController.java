package com.cognizant.springlearn.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.springlearn.exception.EmployeeNotFoundException;
import com.cognizant.springlearn.model.Employee;
import com.cognizant.springlearn.service.EmployeeService;

@RestController
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;
	
	@GetMapping("/employees")
	public List<Employee> getAllEmployees() {
		List<Employee> employeeList = employeeService.getAllEmployees();
		return employeeList;
	}
	
	@PutMapping("/updateEmployee")
	public void updateEmployee(@Valid @RequestBody Employee updateEmployee) throws EmployeeNotFoundException {
		employeeService.updateEmployee(updateEmployee);
	}
	
	@DeleteMapping("/deleteEmployee")
	public List<Employee> deleteEmployee(@RequestBody @Valid Employee deleteEmployee) throws EmployeeNotFoundException {
		List<Employee> employees = employeeService.deleteEmployee(deleteEmployee);
		return employees;
		
	}
}
