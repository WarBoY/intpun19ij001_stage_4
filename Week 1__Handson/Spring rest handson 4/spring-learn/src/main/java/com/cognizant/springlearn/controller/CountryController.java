package com.cognizant.springlearn.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.cognizant.springlearn.Country;
import com.cognizant.springlearn.SpringLearnApplication;
import com.cognizant.springlearn.service.CountryService;

@RestController
@RequestMapping("/countries")
public class CountryController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringLearnApplication.class);
	
	@Autowired
	private CountryService countryService;
	
//	@RequestMapping(value = "/country", method = RequestMethod.GET)
//	public Country getCountryIndia() {
//
//		LOGGER.info("START");
//		ApplicationContext context = new ClassPathXmlApplicationContext("country.xml");
//		Country india = (Country) context.getBean("in", Country.class);
//		LOGGER.info("END");
//		return india;
//
//	}

	@GetMapping
	public List<Country> getAllCountries() {

		LOGGER.info("START");
		ApplicationContext context = new ClassPathXmlApplicationContext("country.xml");
		List<Country> countries = context.getBean("countryList", ArrayList.class);
		System.out.println(countries);
		LOGGER.info("END");
		return countries;

	}

	@GetMapping("/{code}")
	public Country getCountry(@PathVariable("code")String code) {

		LOGGER.info("START");
		Country country = countryService.getCountry(code);
		LOGGER.info("END");
		return country;

	}
	
	@PostMapping
	public Country addCountry(@RequestBody @Valid Country country) {
		LOGGER.info("START");
		// Create validator factory
        LOGGER.info("END");
		return country;
	}
}
