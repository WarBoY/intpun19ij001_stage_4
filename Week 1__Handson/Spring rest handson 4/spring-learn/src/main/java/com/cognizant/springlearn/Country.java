package com.cognizant.springlearn;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Country {

	@NotNull
    @Size(min=2, max=2, message="Country code should be 2 characters")
	private String code;
	private String name;

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringLearnApplication.class);

	public Country() {
		super();
		// TODO Auto-generated constructor stub
		LOGGER.info("Inside Country Constructor.");
	}

	public Country(String code, String name) {
		super();
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		LOGGER.info("Inside Getter Method For code.");
		return code;
	}

	public void setCode(String code) {
		LOGGER.info("Inside Setter Method For code.");
		this.code = code;
	}

	public String getName() {
		LOGGER.info("Inside Getter Method For name.");
		return name;
	}

	public void setName(String name) {
		LOGGER.info("Inside Setter Method For name.");
		this.name = name;
	}

	@Override
	public String toString() {
		return "Country [code=" + code + ", name=" + name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Country other = (Country) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}

}
