package com.cognizant.springlearn.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.validation.Valid;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.cognizant.springlearn.Country;
import com.cognizant.springlearn.exception.CountryNotFoundException;
import com.cognizant.springlearn.exception.EmployeeNotFoundException;
import com.cognizant.springlearn.model.Employee;

@Component
public class EmployeeDao {

	private static List<Employee> EMPLOYEE_LIST = new ArrayList<>();

	public EmployeeDao() {
		super();
		// TODO Auto-generated constructor stub
		
		ApplicationContext context = new ClassPathXmlApplicationContext("employee.xml");
		EMPLOYEE_LIST = context.getBean("employeeList",ArrayList.class);
	}
	
	public List<Employee> getAllEmployees() {
		return EMPLOYEE_LIST;
	}
	
	public void updateEmployee(Employee updateEmployee) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("employee.xml");
		List<Employee> employees = context.getBean("employeeList", ArrayList.class);
		
		if(!employees.contains(updateEmployee)) {
			System.out.println("inside exception");
			throw new EmployeeNotFoundException();
		}else {
			Iterator iterator = employees.iterator();
			while(iterator.hasNext()) {
				Employee employee = (Employee)iterator.next();
				if(employee.equals(updateEmployee)) {
					employees.set(employees.indexOf(employee), updateEmployee);
					System.out.println(employees.get(employees.indexOf(employee)));
					break;
				}
					
			}
		}

	}
	
	public List<Employee> deleteEmployee(Employee deleteEmployee) {
		ApplicationContext context = new ClassPathXmlApplicationContext("employee.xml");
		List<Employee> employees = context.getBean("employeeList", ArrayList.class);
		
		if(!employees.contains(deleteEmployee)) {
			System.out.println("inside exception");
			throw new EmployeeNotFoundException();
		}else {
			Iterator iterator = employees.iterator();
			while(iterator.hasNext()) {
				Employee employee = (Employee)iterator.next();
				if(employee.equals(deleteEmployee)) {
					employees.remove(employee);
					break;
				}
					
			}
		}
		return employees;
		
	}
	
}
