package com.cognizant.springlearn;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
public class SpringLearnApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringLearnApplication.class);
	private static SpringLearnApplication springLearnApplication;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringLearnApplication.class, args);
		System.out.println("Application Started");
		springLearnApplication = new SpringLearnApplication();
		springLearnApplication.displayDate();
		springLearnApplication.displayCountry();
		springLearnApplication.displayCountries();
	}
	
	public void displayDate() {
		
		LOGGER.info("START");
		ApplicationContext context = new ClassPathXmlApplicationContext("date-format.xml");
		SimpleDateFormat format = context.getBean("dateFormat", SimpleDateFormat.class);
		String date = format.format(new Date());
		LOGGER.info(date);
		LOGGER.info("END");
		
	}
	
	public void displayCountry() {
		
		LOGGER.info("START");
		ApplicationContext context = new ClassPathXmlApplicationContext("country.xml");
		Country india = (Country) context.getBean("in", Country.class);
		Country unitedStates= (Country) context.getBean("us", Country.class);
		Country germany = (Country) context.getBean("de", Country.class);
		Country japan = (Country) context.getBean("jp", Country.class);
		Country anotherCountry = context.getBean("in", Country.class);
		LOGGER.debug("Country : {}", india.toString());
		LOGGER.debug("Country : {}", unitedStates.toString());
		LOGGER.debug("Country : {}", germany.toString());
		LOGGER.debug("Country : {}", japan.toString());
		LOGGER.debug("Country : {}", anotherCountry.toString());
		LOGGER.info("END");
		
	}
	
	public void displayCountries() {
		
		LOGGER.info("START");
		ApplicationContext context = new ClassPathXmlApplicationContext("country.xml");
		List<Country> countries = context.getBean("countryList",ArrayList.class);
		LOGGER.debug("Countries : {}", countries);
	}

}
