package com.cognizant.springlearn.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.http.HttpStatus;

@ControllerAdvice
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Country not found")
public class CountryNotFoundException extends RuntimeException{

	public CountryNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	@ExceptionHandler(NoHandlerFoundException.class)
	@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason = "Country not found")
    public void badRequest() {
        
    }
}
