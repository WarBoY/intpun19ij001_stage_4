package com.cognizant.truyum.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.cognizant.truyum.model.Cart;
import com.cognizant.truyum.model.MenuItem;

@Component
public class CartDaoCollectionImpl implements CartDao {
	
	private static HashMap<String , Cart> userCarts=new HashMap<String , Cart>();
	private static List<MenuItem> menuItemList = new ArrayList<>();
	
	public CartDaoCollectionImpl() {
		ApplicationContext context = new ClassPathXmlApplicationContext("truyum.xml");
		menuItemList = context.getBean("menuItemList",ArrayList.class);
	}
	@Override
	public void addCartItem(String userId, long menuItemId) {
		// TODO Auto-generated method stub
		if(userCarts.isEmpty() || !userCarts.containsKey(userId)) {
			MenuItem matchMenuItem = new MenuItem();
			matchMenuItem.setId(menuItemId);
			List<MenuItem> menuItemListForCart = new ArrayList<>();
			Iterator iterator = menuItemList.iterator();
			while(iterator.hasNext()) {
				MenuItem menuItem = (MenuItem) iterator.next();
				if(matchMenuItem.equals(menuItem)) {
					double total = menuItem.getPrice();
					menuItemListForCart.add(menuItem);
					Cart cart = new Cart(menuItemListForCart,total);
					userCarts.put(userId, cart);
					System.out.println(userCarts);
					break;
				}
			}	
		}else {
			Cart cart = userCarts.get(userId);
			List<MenuItem> menuItemListForCart = cart.getMenuItemList();
			double total = cart.getTotal();
			MenuItem matchMenuItem = new MenuItem();
			matchMenuItem.setId(menuItemId);
			Iterator iterator = menuItemList.iterator();
			while(iterator.hasNext()) {
				MenuItem menuItem = (MenuItem) iterator.next();
				if(matchMenuItem.equals(menuItem) && !menuItemListForCart.contains(menuItemId)) {
					total = total + menuItem.getPrice();
					menuItemListForCart.add(menuItem);
					cart.setTotal(total);
					cart.setMenuItemList(menuItemListForCart);
					userCarts.put(userId, cart);
					System.out.println(userCarts);
					break;
				}
			}
		}
		

	}

	@Override
	public List<MenuItem> getAllCartItems(String userId) throws CartEmptyException {
		// TODO Auto-generated method stu
		if(userCarts.containsKey(userId)) {
			Cart cart = userCarts.get(userId);
			List<MenuItem> menuItemList = cart.getMenuItemList();
			return menuItemList;
		}
		return null;
	}

	@Override
	public void deleteCartItem(String userId, long menuItemId) {
		// TODO Auto-generated method stub
		if(userCarts.containsKey(userId)) {
			Cart cart = userCarts.get(userId);
			List<MenuItem> menuItemList = cart.getMenuItemList();
			Iterator iterator = menuItemList.iterator();
			while(iterator.hasNext()) {
				MenuItem menuItem = (MenuItem) iterator.next();
				MenuItem menuItemToDelete = new MenuItem();
				menuItemToDelete.setId(menuItemId);
				if(menuItemToDelete.equals(menuItem)) {
					iterator.remove();
					break;
				}
			}
			
			cart.setMenuItemList(menuItemList);
			userCarts.put(userId, cart);
		}
	}
	
	

}
