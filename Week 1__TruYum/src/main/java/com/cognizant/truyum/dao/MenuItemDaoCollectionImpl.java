package com.cognizant.truyum.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.cognizant.truyum.model.MenuItem;

@Component
public class MenuItemDaoCollectionImpl implements MenuItemDao {
	
	private static List<MenuItem> menuItemList = new ArrayList<>();
	
	public MenuItemDaoCollectionImpl() {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("truyum.xml");
		menuItemList = context.getBean("menuItemList",ArrayList.class);
	}
	
	@Override
	public List<MenuItem> getMenuItemListAdmin() {
		return null;
	}

	@Override
	public List<MenuItem> getMenuItemListCustomer() {
		// TODO Auto-generated method stub
		return  menuItemList;
		
	}

	@Override
	public void modifyMenuItem(MenuItem menuItem) {
		// TODO Auto-generated method stub
		
		Iterator iterator = menuItemList.iterator();
		while(iterator.hasNext()) {
			MenuItem dummyMenuItem = (MenuItem)iterator.next();
			if(dummyMenuItem.equals(menuItem)) {
				menuItemList.set(menuItemList.indexOf(dummyMenuItem), menuItem);
				System.out.println(menuItemList.get(menuItemList.indexOf(dummyMenuItem)));
				break;
			}
		}

	}

	@Override
	public MenuItem getMenuItem(long menuItemId) {
		// TODO Auto-generated method stub
		MenuItem mI = new MenuItem();
		mI.setId(menuItemId);
		Iterator it = menuItemList.iterator();
		while(it.hasNext()) {
			MenuItem menuItem = (MenuItem)it.next();
			if(mI.equals(menuItem)) {
				return menuItem;
			}
		}
		return null;
	}
	
}
