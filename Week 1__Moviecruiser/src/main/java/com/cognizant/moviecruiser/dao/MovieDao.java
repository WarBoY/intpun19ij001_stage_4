package com.cognizant.moviecruiser.dao;

import java.util.List;

import com.cognizant.moviecruiser.model.Favorite;
import com.cognizant.moviecruiser.model.Movie;

public interface MovieDao {

	List<Movie> getAllMovies();
	
	void editMovieAdmin(Movie movie);
	
	List<Movie> relevantMovies();
	
	void AddFavorite(String userId,int movieId);
	
	public void removeFavorite(String userId,int movieId);
	
	public Favorite getFavorite(String userId);
}
