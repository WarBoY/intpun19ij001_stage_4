package com.cognizant.moviecruiser.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.moviecruiser.dao.MovieDao;
import com.cognizant.moviecruiser.model.Favorite;
import com.cognizant.moviecruiser.model.Movie;

@Service
public class MovieService {

	@Autowired
	private MovieDao movieDao;
	
	public List<Movie> getAllMovies() {
		List<Movie> movieList = movieDao.getAllMovies();
		return movieList;
	}
	
	public void editMovieAdmin(Movie movie) {
		movieDao.editMovieAdmin(movie);
	}
	
	public List<Movie> relevantMovies() {
		List<Movie> movieList = movieDao.relevantMovies();
		return movieList;
	}
	
	public void AddFavorite(String userId, int movieId) {
		movieDao.AddFavorite(userId, movieId);
	}
	
	public void removeFavorite(String userId,int movieId) {
		movieDao.removeFavorite(userId, movieId);
	}
	
	public Favorite getFavorite(String userId) {
		Favorite favorite = movieDao.getFavorite(userId);
		return favorite;
	}
}
