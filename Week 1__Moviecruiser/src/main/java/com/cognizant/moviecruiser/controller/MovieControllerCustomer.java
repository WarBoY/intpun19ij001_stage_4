package com.cognizant.moviecruiser.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.moviecruiser.model.Favorite;
import com.cognizant.moviecruiser.model.Movie;
import com.cognizant.moviecruiser.service.MovieService;

@RestController
@RequestMapping("/customer/movies")
public class MovieControllerCustomer {

	@Autowired
	private MovieService movieService;
	
	@GetMapping
	public List<Movie> getAllMovies() {
		List<Movie> movieList = movieService.getAllMovies();
		return movieList;
	}
	
	@GetMapping("/relevant")
	public List<Movie> releventMovies() {
		List<Movie> movieList = movieService.relevantMovies();
		return movieList;
	}
	
	@PutMapping("/{userId}/{movieId}")
	public void AddFavorite(@PathVariable("userId")String userId,@PathVariable("movieId") int movieId) {
		movieService.AddFavorite(userId, movieId);
	}
	
	@DeleteMapping("/{userId}/{movieId}")
	public void removeFavorite(@PathVariable("userId")String userId,@PathVariable("movieId") int movieId) {
		movieService.removeFavorite(userId, movieId);
	}
	
	@GetMapping("/{userId}/favorite")
	public Favorite getFavorite(@PathVariable("userId")String userId) {
		Favorite favorite = movieService.getFavorite(userId);
		return favorite;
	}
}
