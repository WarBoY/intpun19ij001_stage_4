package com.cognizant.moviecruiser.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.moviecruiser.model.Movie;
import com.cognizant.moviecruiser.service.MovieService;

@RestController
@RequestMapping("/admin/movies")
public class MovieControllerAdmin {

	@Autowired
	private MovieService movieService;
	
	@GetMapping
	public List<Movie> getAllMovies() {
		List<Movie> movieList = movieService.getAllMovies();
		return movieList;
	}
	
	@PutMapping
	public void editMovieAdmin(@RequestBody Movie movie) {
		movieService.editMovieAdmin(movie);
	}
}
